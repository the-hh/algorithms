# Algorithms

/BigONotation :

Maybe I found a new algorithm for calculating Fibbonacci numbers. Haven't found a similar approach so far. Am I right or wrong? If anyone knows, I'd appreciate it if you let me know - find me❕
As long as no one has proven the opposite it is my little mathematical invention 😊 A piece of mathematical beauty.

About the algorithm in brief:
Its time complexity is O(logn).
The math inside the algorithm provides an accurate and correct answer.
The code for this algorithm looks elegantly clean and simple. There is currently no mathematically correct proof of this, but tests prove that the algorithm works correctly.
I tried to write this serial calculation like in math textbooks and the result was no longer clean and simple. I am a hobby mathematician; a professional mathematician can perhaps describe it more simply...

Some references to the theory of Fibonacci numbers:
https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
https://r-knott.surrey.ac.uk/Fibonacci/fibFormula.html
