package org.BigONotation.function;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FibonacciTest {

    @Test
    public void canGetValueAtNWithBinetsFormula() {
        assertEquals(BinetsFormula.getValueAtN(1), 1);
        assertEquals(BinetsFormula.getValueAtN(2), 1);
        assertEquals(BinetsFormula.getValueAtN(3), 2);
        assertEquals(BinetsFormula.getValueAtN(31), 1346269);
        assertEquals(BinetsFormula.getValueAtN(19), 4181);
        assertEquals(BinetsFormula.getValueAtN(24), 46368);
        assertEquals(BinetsFormula.getValueAtN(21), 10946);
    }

    @Test
    public void canGetValueAtNWithFibonacciWithRecursion() {
        assertEquals(FibonacciWithRecursion.getValueAtN(1), 1);
        assertEquals(FibonacciWithRecursion.getValueAtN(2), 1);
        assertEquals(FibonacciWithRecursion.getValueAtN(3), 2);
        assertEquals(FibonacciWithRecursion.getValueAtN(31), 1346269);
        assertEquals(FibonacciWithRecursion.getValueAtN(19), 4181);
        assertEquals(FibonacciWithRecursion.getValueAtN(24), 46368);
        assertEquals(FibonacciWithRecursion.getValueAtN(21), 10946);
    }

    @Test
    public void canGetValueAtNWithLinearFibonacci() {
        assertEquals(LinearFibonacci.getValueAtN(1), 1);
        assertEquals(LinearFibonacci.getValueAtN(2), 1);
        assertEquals(LinearFibonacci.getValueAtN(3), 2);
        assertEquals(LinearFibonacci.getValueAtN(31), 1346269);
        assertEquals(LinearFibonacci.getValueAtN(19), 4181);
        assertEquals(LinearFibonacci.getValueAtN(24), 46368);
        assertEquals(LinearFibonacci.getValueAtN(21), 10946);
    }

    @Test
    public void canGetValueAtNWithLogarithmicFibonacci() {
        assertEquals(LogarithmicFibonacci.getValueAtN(1), 1);
        assertEquals(LogarithmicFibonacci.getValueAtN(2), 1);
        assertEquals(LogarithmicFibonacci.getValueAtN(3), 2);
        assertEquals(LogarithmicFibonacci.getValueAtN(31), 1346269);
        assertEquals(LogarithmicFibonacci.getValueAtN(19), 4181);
        assertEquals(LogarithmicFibonacci.getValueAtN(24), 46368);
        assertEquals(LogarithmicFibonacci.getValueAtN(21), 10946);
    }

}