package org.BigONotation.function;

public class LogarithmicFibonacci {

    public static double getValueAtN(int n) {

        double[] base = new double[] {0, 0, 1};   // Initialization to calculate sequence of Fibonacci numbers at 2^n
        double[] interim = new double[] {1, 0, 1};   // Initialization:  F(-1) = 1 ,   F(0) = 0 ,   F(1) = 1
        double temp;

        while (n >= 1) {
            // base[1] = F(2^n) and base[0] = F(2^n-1) and base[2] = F(2^n+1)
            // During 1. cycle: base[1] = F(2^0) = F(1) =  1
            // During 2. cycle: base[1] = F(2^1) = F(2) =  1
            // During 3. cycle: base[1] = F(2^2) = F(4) =  3
            // During 4. cycle: base[1] = F(2^4) = F(8) = 21
            // ...
            temp = base[0] * base[0];
            base[0] = base[1] * base[1] + temp;
            base[1] = base[2] * base[2] - temp;
            base[2] = base[0] + base[1];

            // cycle by cycle "converting" a decimal number n to a binary number:
            // 1) if (n % 2 == 1)
            //    true: calculation of intermediate Fibonacci number interim[1] and values nearby
            //    false: skip
            // 2) n = n / 2;
            if (n % 2 == 1) {
                temp = interim[0] * base[0];
                interim[0] = interim[1] * base[1] + temp;
                interim[1] = interim[2] * base[2] - temp;
                interim[2] = interim[0] + interim[1];
            }
            n = n / 2;
        }
        return interim[1];
    }
}