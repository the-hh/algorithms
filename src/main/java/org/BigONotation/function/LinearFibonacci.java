package org.BigONotation.function;

public abstract class LinearFibonacci {

    public static double getValueAtN(int n) {
        double firstTerm = 0;
        double secondTerm = 1;
        double nextTerm = 0;
        if (n == 0 || n == 1) {
            return n;
        } else {
            for (int i = 2; i <= n; ++i) {
                nextTerm = firstTerm + secondTerm;
                firstTerm = secondTerm;
                secondTerm = nextTerm;
            }
        }
        return nextTerm;
    }
}
