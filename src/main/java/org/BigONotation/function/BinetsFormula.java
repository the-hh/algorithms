package org.BigONotation.function;

public class BinetsFormula {
    public static double getValueAtN(int n) {
        double sqrt5 = Math.sqrt(5);
        return Math.round((Math.pow((1 + sqrt5)/2, n) - Math.pow((1 - sqrt5)/2, n))/ sqrt5);
    }
}
