package org.BigONotation.function;

public abstract class FibonacciWithRecursion {
    public static double getValueAtN(int n) {
        if (n == 0 || n == 1) {
            return n;
        } else {
            return getValueAtN(n - 1) + getValueAtN(n - 2);
        }
    }

}
