package org.BigONotation;

import org.BigONotation.function.BinetsFormula;
import org.BigONotation.function.FibonacciWithRecursion;
import org.BigONotation.function.LinearFibonacci;
import org.BigONotation.function.LogarithmicFibonacci;

import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;

public class BigONotation {

    // The FROM limit is included
    public static final int FROM = 1;

    // The TO limit is excluded
    // By using Double for calculating Fibonacci value the MAX TO is 1475
    public static final int TO = 55;


    public static void main(String[] args) {


        System.out.println("\n O(1)   'Constant-size' Fibonacci: ");
        IntToDoubleFunction getValueByUsingGoldenRatioFunction = BinetsFormula::getValueAtN;
        exexuteFunction(getValueByUsingGoldenRatioFunction);

        System.out.println("\n ***********************************************************************");
        System.out.println("O(log n)   Logarithmic Fibonacci: ");
        IntToDoubleFunction getValueByUsingLogaritmicFunction = LogarithmicFibonacci::getValueAtN;
        exexuteFunction(getValueByUsingLogaritmicFunction);
        System.out.println("************************************************************************");

        System.out.println("\n O(n)   Linear Fibonacci: ");
        IntToDoubleFunction getValueByUsingLinearFunction = LinearFibonacci::getValueAtN;
        exexuteFunction(getValueByUsingLinearFunction);

        System.out.println("\n O(c^{n})   Exponential Fibonacci: ");
        IntToDoubleFunction getValueByUsingExponentialFunction = FibonacciWithRecursion::getValueAtN;
        exexuteFunction(getValueByUsingExponentialFunction);


    }

    private static void exexuteFunction(IntToDoubleFunction getValueByUsingLogaritmicFunction) {
//        long begin = System.currentTimeMillis();
        IntStream.range(FROM, TO)
                .mapToDouble(getValueByUsingLogaritmicFunction)
                .forEach(f -> System.out.print(f + "   "));
        System.out.println();
//        long end = System.currentTimeMillis();
//        long time = end-begin;
//        System.out.println("\n Elapsed Time: "+time +" milli seconds");
    }
}
